import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { LocalStorageService } from "@microbizllc/application-core";

import { LoginService } from "./services/login.service";

@Component({
  selector: 'mbiz-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Output() onLoggedIn: EventEmitter<boolean> = new EventEmitter();

  constructor( private route: ActivatedRoute,
               private loginService: LoginService,
               private localStorageService: LocalStorageService,
               private router: Router) { }

  model = {
    client_id:'TestClient',
    client_secret:'TestSecret',
  };

  loading:boolean = false;
  error:any = null;

  ngOnInit() {
  }

  login(){
    this.loading = true;
    this.error = null;
    this.loginService.login(this.model).subscribe(
        resp => {
          this.loading = false;
          this.localStorageService.setJSON( 'auth', resp.json() );
          this.onLoggedIn.emit( true );
        },
        err => {
          this.loading = false;
          this.error = err.json && err.json();
          this.onLoggedIn.emit( false );
        }
    );
  }

  onSubmit(e){
    e.preventDefault();
    this.login();
  }

}

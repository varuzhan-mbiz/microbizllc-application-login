import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { DialogModule } from "@progress/kendo-angular-dialog";

import { CoreModule } from '@microbizllc/application-core';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { LoginService } from "./services/login.service";

@NgModule( {
    imports:      [
        BrowserModule,
        CommonModule,
        FormsModule,
        DialogModule,
        LoginRoutingModule,
        CoreModule,
    ],
    declarations: [ LoginComponent ],
    providers:    [ LoginService ],
    bootstrap:    [ LoginComponent ]
} )
export class LoginModule {}

export * from './login.component';
export * from './login-routing.module';

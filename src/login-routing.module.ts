import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./login.component";

//import { AuthGuardService } from "@microbizllc/application-core";
//import { AuthGuardService } from "../../../modules/microbizllc-application-core";

export const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [ /*AuthGuardService*/ ] }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }

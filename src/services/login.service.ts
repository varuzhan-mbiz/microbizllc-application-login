import { Injectable } from '@angular/core';
import { Http, ResponseOptions, Response, Headers } from "@angular/http";
import { Observable } from "rxjs";


import { ApiUrlService  } from "@microbizllc/application-core";


@Injectable()
export class LoginService {

  constructor( private http:Http, private apiUrlService:ApiUrlService ) { }

  login( data?: any ) {

    let http        = this.http;
    let url         = this.apiUrlService.login;
    let headers     = new Headers();
    let credentials = `Basic ${ btoa( `${data.client_id}:${data.client_secret}` ) }`;
    headers.append( 'Authorization', credentials );
    //headers.append( 'grant_type', 'client_credentials' );

    return http.post( url, {
        grant_type: 'client_credentials'
    }, {headers});

  }

  logout(){
    localStorage.removeItem( 'auth' );
  }

}

import { NgModule }    from '@angular/core';
import { LoginModule } from "./src/login.module";
import { BrowserModule } from "@angular/platform-browser";


@NgModule( {
    imports: [
        LoginModule
    ],
    exports:      [
        LoginModule
    ]
} )
export class MbizLoginModule {}

export * from './src/login.module';